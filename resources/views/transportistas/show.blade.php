@extends('layouts.master') 
 
@section('titulo')
  Mostrar transportista
@endsection 
 
@section('contenido')
  <div class="row">
    <div class="col-sm-3">
      <img src="{{ asset('/assets/imagenes/transportistas/'.$transportista->imagen) }}" style="height:200px"/>
    </div>
    <div class="col-sm-9">
      <h2>{{ $transportista->nombre . $transportista->apellidos }})</h2>
      <h4>Años conduciendo: </h4>
      <p>{{ $transportista->getEdad() }}</p>
      <h4>Empresas: </h4>
      <p>{{ $transportista->empresas }}</p>
      <h4>Paquetes: </h4>
      <p>{{ $transportista->paquetes }}</p>

      <a class="btn btn-primary" href="#" role="button">Entregar todo</a>
      <a class="btn btn-outline-secondary" href="#" role="button">Marcar como no entregado</a>
    </div>
  </div> 
@endsection 