<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresaTransportistaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_transportista', function (Blueprint $table) {            
            $table->unsignedBigInteger('empresa_id');
            $table->unsignedBigInteger('transportista_id');           
            $table->timestamps();

            $table->primary(array('empresa_id', 'transportista_id'));

            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->foreign('transportista_id')->references('id')->on('transportistas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transportista_empresa');
    }
}
