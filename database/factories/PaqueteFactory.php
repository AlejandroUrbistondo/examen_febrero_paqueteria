<?php

namespace Database\Factories;

use App\Models\Paquete;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaqueteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Paquete::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $direccion = $this->faker->streetPrefix . ' ' . 
                        $this->faker->cityPrefix . ' ' . 
                        $this->faker->secondaryAddress . ' ' . 
                        $this->faker->state . ' ' . 
                        $this->faker->community;
        return [
            'direccion_entrega' => $direccion,
            'imagen' => 'paquete_por_defecto.jpg',
            'transportista_id' => rand(1,9),
        ];
    }
}
