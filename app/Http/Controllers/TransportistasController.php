<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Transportista;

class TransportistasController extends Controller
{
    public function home()
    {
        return redirect()->action([TransportistasController::class,'index']);
    }

    public function index()
    {
    	$transportistas = Transportista::all();
        return view('transportistas.index', compact('transportistas'));
    }

    public function show(Transportista $transportista)
    {
        return view('transportistas.show', compact('transportista'));
    }
}
