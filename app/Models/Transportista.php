<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Transportista extends Model
{
    use HasFactory;

    protected $table = 'transportistas';

    public function getEdad()
    {
        $fecha_formateada = Carbon::parse($this->fechaObtencionPermiso);
        return $fecha_formateada->diffInYears(Carbon::now());
    }

    public function paquetes()
    {
    	return $this->hasMany(Paquete::class);
    }

    public function empresas()
    {
    	return $this->belongsToMany(Empresa::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
