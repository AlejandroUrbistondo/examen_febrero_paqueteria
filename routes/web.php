<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\TransportistasController;
//use App\Http\Controllers\PaquetesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', [TransportistasController::class,'home'])->name('home');

Route::get('transportistas', [TransportistasController::class,'index'])->name('transportistas.index');

Route::get('transportistas/{transportista}', [TransportistasController::class,'show'])->name('transportistas.show');
